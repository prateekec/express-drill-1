const express= require('express');
const fs= require('fs');
const uuid=require('uuid4');
const app =express();

const port= process.env.PORT || 5432;

app.get('/html', (req,res) => {
    res.sendFile('resources/htmlDocument.html',{root:'./'});
});

app.get('/json', (req,res) => {
    res.sendFile('resources/jsonDocument.json',{root:'./'});
});

app.get('/uuid', (req,res) => {
    let id=uuid();
    res.send(id);
});
app.get('/status/:code',(req,res) => {
    res.send(req.params.code);
});

app.get('/delay/:time',(req,res) => {
    let time=req.params.time;
    setTimeout(() => {
        res.send(`Delayed by ${time} second/s.`)
    }, time*1000);
});
app.listen(port,() => {
    console.log(`listening`);
});